package br.com.ozcorp;

public class Departamento{
	
	String nomeDep;
	String sigla;
	Cargo cargo;
	public Departamento(String nomeDep, String sigla, Cargo cargo) {
		super();
		this.nomeDep = nomeDep;
		this.sigla = sigla;
		this.cargo = cargo;
	}
	public String getNomeDep() {
		return nomeDep;
	}
	public void setNomeDep(String nomeDep) {
		this.nomeDep = nomeDep;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	

	
}
