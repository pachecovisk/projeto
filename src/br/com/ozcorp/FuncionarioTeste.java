package br.com.ozcorp;

public class FuncionarioTeste {
	public static void main(String[] args) {
		
	
		Cargo engenheiro = new Cargo("Engenheiro", 2_500.00);
		Cargo diretor = new Cargo("Diretor", 5_000.00);
		Cargo secretario = new Cargo("Secretaria", 1_500.00);
		Cargo analista = new Cargo("Analista", 4_000.00);
		Cargo gerente = new Cargo("Gerente", 3_500.00);
		
		
		Departamento financeiro = new Departamento("Financeiro", "FIN", diretor);
		Departamento administrativo = new Departamento("Administrativo", "ADM", gerente);
		Departamento trabalhista = new Departamento("Trabalhista", "TRAB", engenheiro);
		Departamento ti = new Departamento("T�cnico de Inform�tica", "TI", analista);
		Departamento secretaria = new Departamento("Secretaria", "SCT", secretario);
		
		Funcionario Joao = new Funcionario("Jo�o", "123141425342", "21412414213", "12445", "joao@email.com", "123", 4, TipoSanguineo.A_NEGATIVO, Sexo.MASCULINO, ti);
		
		System.out.println("Dados do funcion�rio: ");
		System.out.println("Nome            : " + Joao.getNome());
		System.out.println("RG              : " + Joao.getRg());
		System.out.println("CPF             : " + Joao.getCpf());
		System.out.println("Matr�cula       : " + Joao.getMatricula());
		System.out.println("E-Mail          : " + Joao.getEmail());
		System.out.println("Senha           : " + Joao.getSenha());
		System.out.println("Cargo           : " + Joao.getDepartamento().getCargo().getTitulo());
		System.out.println("Sal�rio base    : " + Joao.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo sangu�neo  : " + Joao.getTipoSanguineo());
		System.out.println("Sexo            : " + Joao.getSexo());
	}
}
						